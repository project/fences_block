<?php

namespace Drupal\Tests\fences_block\Functional;

use Drupal\Tests\block\Functional\BlockTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group fences_block
 */
class FencesBlockFunctionalCustomBlockStarkTest extends BlockTestBase {

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'block',
    'filter',
    'test_page_test',
    'help',
    'block_test',
    'fences',
    'fences_block',
    'block_content',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Test fences block settings on custom blocks.
   */
  public function testCustomBlockAllSettings() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Create block type:
    $this->drupalGet('/admin/structure/block-content/add');
    $page->fillField('edit-label', 'test_block_type');
    $page->fillField('edit-id', 'test_block_type');
    $page->fillField('edit-description', 'test 123');
    $page->pressButton('edit-submit');
    // Create block:
    $this->drupalGet('/block/add/test_block_type');
    $page->fillField('edit-info-0-value', 'test_block');
    $page->fillField('edit-body-0-value', 'test 123');
    $page->pressButton('edit-configure-block');
    // We will get redirected to the settings page of the block here, so we can
    // set the fences_block settings right away:
    $page->fillField('edit-region', 'header');
    $this->submitForm([
      'settings[fences][sections][wrapper][element]' => 'article',
      'settings[fences][sections][wrapper][classes]' => 'wrapper-test-class',
      'settings[fences][sections][label][element]' => 'h1',
      'settings[fences][sections][label][classes]' => 'test-label-class',
      'settings[fences][sections][content][element]' => 'article',
      'settings[fences][sections][content][classes]' => 'test-content-class',
    ], 'Save block');

    // Debug block settings:
    // $this->drupalGet('/admin/structure/block/manage');.
    // Test on the frontpage:
    $this->drupalGet('<front>');
    $session->pageTextContains('Test page text.');

    $session->elementExists('css', '#block-stark-test-block');
    $session->elementExists('css', 'article#block-stark-test-block');
    $session->elementAttributeContains('css', '#block-stark-test-block', 'class', 'wrapper-test-class');
    $session->elementExists('css', '#block-stark-test-block > h1.test-label-class');
    $session->elementTextEquals('css', '#block-stark-test-block > h1.test-label-class', 'test_block');
    $session->elementExists('css', '#block-stark-test-block > article.test-content-class');
  }

}
