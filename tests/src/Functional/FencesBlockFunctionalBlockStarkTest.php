<?php

namespace Drupal\Tests\fences_block\Functional;

use Drupal\Tests\block\Functional\BlockTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group fences_block
 */
class FencesBlockFunctionalBlockStarkTest extends BlockTestBase {

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with authenticated permission and newly added permission.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $userWithPermission;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;


  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'block',
    'filter',
    'test_page_test',
    'help',
    'block_test',
    'fences',
    'fences_block',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();

    $this->user = $this->drupalCreateUser(['administer blocks']);
    $this->userWithPermission = $this->drupalCreateUser([
      'administer blocks',
      'edit fences block formatter settings',
    ]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Helper function to place a system block with certain settings.
   *
   * @param string $block_id
   *   The system block id.
   * @param string $region
   *   The region where the block is placed.
   * @param bool $labelVisible
   *   Display the label?
   * @param array $settings
   *   Override default test settings.
   */
  protected function placeSystemBlock(string $block_id, string $region = 'content', bool $labelVisible = TRUE, array $settings = []): void {
    $page = $this->getSession()->getPage();
    // Edit block:
    $this->drupalGet('/admin/structure/block/add/' . $block_id . '/' . $this->defaultTheme);
    $page->fillField('edit-region', $region);

    $formValues = [
      'settings[fences][sections][wrapper][element]' => $settings['wrapper']['element'] ?? 'article',
      'settings[fences][sections][wrapper][classes]' => $settings['wrapper']['classes'] ?? 'test-wrapper-class',
      'settings[fences][sections][label][element]' => $settings['label']['element'] ?? 'h3',
      'settings[fences][sections][label][classes]' => $settings['label']['classes'] ?? 'test-label-class',
      'settings[fences][sections][content][element]' => $settings['content']['element'] ?? 'em',
      'settings[fences][sections][content][classes]' => $settings['content']['classes'] ?? 'test-content-class',
    ];
    if ($labelVisible) {
      $formValues['settings[label_display]'] = 'visible';
    }

    $this->submitForm($formValues, 'Save block');
  }

  /**
   * Tests the poweredbydrupal block.
   */
  public function testPoweredByBlock(): void {
    $session = $this->assertSession();

    // Place the block:
    $this->placeSystemBlock('system_powered_by_block', 'sidebar_second', TRUE, [
      'wrapper' => [
        'element' => 'span',
      ],
      'label' => [
        'element' => 'strong',
      ],
      'content' => [
        'element' => 'em',
      ],
    ]);

    // See core/modules/block/tests/src/Functional/BlockSystemBrandingTest.php.
    // Set a site slogan.
    $this->config('system.site')
      ->set('slogan', 'Community plumbing')
      ->save();

    // Debug block settings:
    // $this->drupalGet('/admin/structure/block/manage/poweredbydrupal');.
    // Test the frontpage:
    $this->drupalGet('<front>');
    $session->pageTextContains('Test page text.');

    $session->pageTextContains('Powered by Drupal');
    $session->elementExists('css', '#block-stark-poweredbydrupal');
    $session->elementExists('css', 'span#block-stark-poweredbydrupal');
    $session->elementExists('css', 'span#block-stark-poweredbydrupal.test-wrapper-class');
    $session->elementExists('css', '#block-stark-poweredbydrupal > em.test-content-class');
    $session->elementExists('css', '#block-stark-poweredbydrupal > strong');
    $session->elementExists('css', 'aside > div > span#block-stark-poweredbydrupal.test-wrapper-class > em.test-content-class > span > a');
    $session->elementTextEquals('css', '#block-stark-poweredbydrupal em.test-content-class', 'Powered by Drupal');
  }

  // DOES NOT WORK BY DESIGN, SEE FencesBlockFunctionalBlockStarkDysfunctional.php
  // and https://www.drupal.org/project/fences_block/issues/3304737
  // /**
  // * Test system branding block.
  // */
  // public function testSystemBrandingBlockWithLabel(): void {
  //   $session = $this->assertSession();
  // // Place the block:
  //   $this->placeSystemBlock('system_branding_block', 'header', TRUE, []);
  // // Set a site slogan.
  //   // See core/modules/block/tests/src/Functional/BlockSystemBrandingTest.php.
  //   $this->config('system.site')
  //     ->set('slogan', 'Community plumbing')
  //     ->save();
  // // Debug block settings:
  //   $this->drupalGet('/admin/structure/block/manage/sitebranding');
  // // Test the frontpage:
  //   $this->drupalGet('<front>');
  //   $session->pageTextContains('Test page text.');
  // $session->pageTextContains('Community plumbing');
  //   $session->elementExists('css', '#block-stark-sitebranding');
  //   $session->elementExists('css', 'article#block-stark-sitebranding');
  //   $session->elementAttributeContains('css', '#block-stark-sitebranding', 'class', 'test-wrapper-class');
  //   $session->elementExists('css', '#block-stark-sitebranding > h3.test-label-class');
  //   $session->elementTextEquals('css', '#block-stark-sitebranding > h3.test-label-class', 'Site branding');
  //   $session->elementExists('css', '#block-stark-sitebranding > article.test-content-class');
  // }.

  /**
   * Test system page title block.
   */
  public function testPageTitleBlock(): void {
    $session = $this->assertSession();

    // Place the block:
    $this->placeSystemBlock('page_title_block', 'content', FALSE, []);

    // Debug block settings:
    // $this->drupalGet('/admin/structure/block/manage/pagetitle');.
    // Test on frontpage:
    $this->drupalGet('<front>');
    $session->pageTextContains('Test page text.');

    $session->elementExists('css', '#block-stark-pagetitle');
    $session->elementExists('css', 'article#block-stark-pagetitle');
    $session->elementAttributeContains('css', 'article#block-stark-pagetitle', 'class', 'test-wrapper-class');
    // We don't display a label:
    $session->elementNotExists('css', '#block-stark-pagetitle h3.test-label-class');
    $session->elementExists('css', 'article#block-stark-pagetitle.test-wrapper-class em.test-content-class');
  }

  // DOES NOT WORK BY DESIGN, SEE FencesBlockFunctionalBlockStarkDysfunctional.php
  // and https://www.drupal.org/project/fences_block/issues/3304737
  // /**
  //  * Test system main content block.
  //  */
  // public function testSystemMainBlock(): void {
  //   $session = $this->assertSession();
  // // Place the block:
  //   $this->placeSystemBlock('system_main_block', 'content', FALSE, []);
  // // Debug block settings:
  //   $this->drupalGet('/admin/structure/block/manage/mainpagecontent');
  // // Test on frontpage:
  //   $this->drupalGet('<front>');
  //   $session->pageTextContains('Test page text.');
  // $session->elementExists('css', '#block-stark-mainpagecontent');
  //   $session->elementExists('css', 'article#block-stark-mainpagecontent');
  //   $session->elementAttributeContains('css', '#block-stark-mainpagecontent', 'class', 'test-wrapper-class');
  //   // We don't display a label:
  //   // $session->elementExists('css', '#block-stark-mainpagecontent > h3.test-label-class');
  //   // $session->elementTextEquals('css', '#block-stark-mainpagecontent > h3.test-label-class', 'Test page');
  //   $session->elementExists('css', '#block-stark-mainpagecontent > article.test-content-class');
  // }.

  /**
   * Tests multiple blocks placed on the same page in different regions.
   */
  public function testSystemBlocksCombined(): void {
    $session = $this->assertSession();

    // Place the block:
    $this->placeSystemBlock('system_powered_by_block', 'sidebar_second', TRUE, []);
    $this->placeSystemBlock('system_branding_block', 'header', TRUE, []);
    $this->placeSystemBlock('page_title_block', 'content', TRUE, []);
    $this->placeSystemBlock('system_main_block', 'content', TRUE, []);

    // Debug block settings:
    // $this->drupalGet('/admin/structure/block/manage/mainpagecontent');.
    // Test on frontpage:
    $this->drupalGet('<front>');
    $session->pageTextContains('Test page text.');

    // @todo Add further tests for all blocks!
    // Expected to work:
    $session->elementExists('css', '#block-stark-poweredbydrupal');
    $session->elementExists('css', 'article#block-stark-poweredbydrupal');

    // Expected NOT to work:
    $session->elementExists('css', '#block-stark-sitebranding');
    $session->elementNotExists('css', 'article#block-stark-sitebranding');

    // Expected to work:
    $session->elementExists('css', '#block-stark-pagetitle');
    $session->elementExists('css', 'article#block-stark-pagetitle');

    // Expected to work:
    $session->elementExists('css', '#block-stark-mainpagecontent');
    $session->elementExists('css', 'article#block-stark-mainpagecontent');
  }

  /**
   * Tests the "edit fences block formatter settings" permission.
   */
  public function testEditFencesBlockFormatterSettingsPermission() {
    $session = $this->assertSession();
    $this->placeSystemBlock('page_title_block', 'content', TRUE, []);

    // Check permission as admin:
    $this->drupalGet('/admin/structure/block/add/page_title_block/' . $this->defaultTheme);
    $session->elementExists('css', '#edit-settings-fences-sections');
    $session->pageTextContains('Fences Block');
    $this->drupalLogout();
    // Check permission as authenticated user without permission:
    $this->drupalLogin($this->user);
    $this->drupalGet('/admin/structure/block/add/page_title_block/' . $this->defaultTheme);
    $session->elementNotExists('css', '#edit-settings-fences-sections');
    $session->pageTextNotContains('Fences Block');
    // Check permission as authenticated user with module's permission:
    $this->drupalLogin($this->userWithPermission);
    $this->drupalGet('/admin/structure/block/add/page_title_block/' . $this->defaultTheme);
    $session->elementExists('css', '#edit-settings-fences-sections');
    $session->pageTextContains('Fences Block');
  }

}
