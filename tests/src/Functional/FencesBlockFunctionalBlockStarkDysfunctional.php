<?php

namespace Drupal\Tests\fences_block\Functional;

use Drupal\Tests\block\Functional\BlockTestBase;

/**
 * This class ensures that we're NOT overwriting theme overwrites of
 * block.html.twig with out custom block--fences.html.twig.
 * See https://www.drupal.org/project/fences_block/issues/3304737
 * for details!
 *
 * @group fences_block
 */
class FencesBlockFunctionalBlockStarkDisfunctional extends BlockTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'block',
    'filter',
    'test_page_test',
    'help',
    'block_test',
    'fences',
    'fences_block',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Setup is done in parent class already!
  }

  /**
   * Test system branding block.
   */
  public function testSystemBrandingBlockWithLabel(): void {
    $session = $this->assertSession();

    // Place the block:
    $this->placeSystemBlock('system_branding_block', 'header', TRUE, []);

    // Set a site slogan.
    // See core/modules/block/tests/src/Functional/BlockSystemBrandingTest.php.
    $this->config('system.site')
      ->set('slogan', 'Community plumbing')
      ->save();

    // Debug block settings:
    // $this->drupalGet('/admin/structure/block/manage/sitebranding');.
    // Test the frontpage:
    $this->drupalGet('<front>');
    $session->pageTextContains('Test page text.');

    // Expected:
    // BEGIN OUTPUT from 'core/modules/system/templates/block--system-branding-block.html.twig'.
    $session->pageTextContains('Community plumbing');
    $session->elementExists('css', '#block-sitebranding');
    // Overwriting the wrapper does NOT work due to missing template overwrite:
    $session->elementNotExists('css', 'article#block-sitebranding');
    // Overwriting the class also works with the core template:
    $session->elementAttributeContains('css', '#block-sitebranding', 'class', 'test-wrapper-class');
    // Overwriting the label does NOT work due to missing template overwrite:
    $session->elementNotExists('css', '#block-sitebranding > h3.test-label-class');
    // Overwriting the class also works with the core template:
    $session->elementTextEquals('css', '#block-sitebranding > .test-label-class', 'Site branding');
  }

  /**
   * Test system main content block.
   */
  public function testSystemMainBlock(): void {
    $session = $this->assertSession();

    // Place the block:
    $this->placeSystemBlock('system_main_block', 'content', FALSE, []);

    // Debug block settings:
    // $this->drupalGet('/admin/structure/block/manage/mainpagecontent');.
    // Test on frontpage:
    $this->drupalGet('<front>');
    $session->pageTextContains('Test page text.');

    // Overwriting the wrapper does NOT work due to missing template overwrite:
    $session->elementNotExists('css', 'article#block-mainpagecontent');
    // Overwriting the class also works with the core template:
    $session->elementAttributeContains('css', '#block-mainpagecontent', 'class', 'test-wrapper-class');
  }

  /**
   * Helper function to place a system block with certain settings.
   *
   * @param string $block_id
   *   The system block id.
   * @param string $region
   *   The region where the block is placed.
   * @param bool $labelVisible
   *   Display the label?
   * @param array $settings
   *   Override default test settings.
   */
  protected function placeSystemBlock(string $block_id, string $region = 'content', bool $labelVisible = TRUE, array $settings = []): void {
    $page = $this->getSession()->getPage();
    // Edit block:
    $this->drupalGet('/admin/structure/block/add/' . $block_id . '/' . $this->defaultTheme);
    $page->fillField('edit-region', $region);

    $formValues = [
      'settings[fences][sections][wrapper][element]' => $settings['wrapper']['element'] ?? 'article',
      'settings[fences][sections][wrapper][classes]' => $settings['wrapper']['classes'] ?? 'test-wrapper-class',
      'settings[fences][sections][label][element]' => $settings['label']['element'] ?? 'h3',
      'settings[fences][sections][label][classes]' => $settings['label']['classes'] ?? 'test-label-class',
      'settings[fences][sections][content][element]' => $settings['content']['element'] ?? 'em',
      'settings[fences][sections][content][classes]' => $settings['content']['classes'] ?? 'test-content-class',
    ];
    if ($labelVisible) {
      $formValues['settings[label_display]'] = 'visible';
    }

    $this->submitForm($formValues, 'Save block');
  }

}
