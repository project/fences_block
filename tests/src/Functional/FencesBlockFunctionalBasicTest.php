<?php

namespace Drupal\Tests\fences_block\Functional;

use Drupal\Tests\block\Functional\BlockTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group fences_block
 */
class FencesBlockFunctionalBasicTest extends BlockTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'block',
    'filter',
    'test_page_test',
    'help',
    'block_test',
    'fences',
    'fences_block',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Setup is done in parent class already!
    // Admin role for the admin:
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
  }

  /**
   * Tests if the module installation, won't break the site.
   */
  public function testInstallation(): void {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    // Ensure the status code is success:
    $session->statusCodeEquals(200);
    // Ensure the correct test page is loaded as front page:
    $session->pageTextContains('Test page text.');
  }

  /**
   * Tests if uninstalling the module, won't break the site.
   */
  public function testUninstallation(): void {
    // Go to uninstallation page an uninstall fences_block:
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/modules/uninstall');
    $session->statusCodeEquals(200);
    $page->checkField('edit-uninstall-fences-block');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);

    // Confirm uninstall:
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The selected modules have been uninstalled.');

    // Retest the frontpage:
    $this->drupalGet('<front>');
    // Ensure the status code is success:
    $session->statusCodeEquals(200);
    // Ensure the correct test page is loaded as front page:
    $session->pageTextContains('Test page text.');
  }

}
